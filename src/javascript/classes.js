class Annonce {
  _id;
  _text;
  _user;

  constructor(id, text, user) {
    this._id = id;
    this._text = text;
    this._user = user;
  }

  user() {
    return this._user;
  }

  id() {
    return this._id;
  }

  text() {
    return this._text;
  }
}

class Utilisateur {
  _username;
  _password;
  _annonces = [];
  _admin;

  constructor(username, password, admin) {
    this._username = username;
    this._password = password;
    this._admin = admin;
  }
  username() {
    return this._username;
  }
  password() {
    return this._password;
  }

  admin() {
    return this._admin;
  }
}

class Erreur{
  _nom;
  _message;

  constructor(nom,message){
    this._nom = "Nom de l'erreur: " + nom;
    this._message = "Message de l'erreur: "+ message;
  }

  nom(){
    return this._nom;
  }

  message(){
    return this._message;
  }
}
