var userConnecte;
var listeErreurs = [];
var _listUsers = [];
var _listeAnnonce = [
  new Annonce(1, "4 et demi à louer, tout inclus 1200$ / mois", "test"),
];

function _init() {
  try{
    createUser("admin", "admin", true);
  initAnnonces();
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
  
}

function connexion() {
  try{
      let username = document.getElementById("nom-utilisateur").value;
  let password = document.getElementById("mot-de-passe").value;

  var user = getUser(username, password);

  if (user) {
    userConnecte = new Utilisateur(
      user.username(),
      user.password(),
      user.admin()
    );
    afficherAppUser();
    if (user.admin()) {
      afficherAppAdmin();
    }
  } else {
    // TODO : Erreur user ou mot de passe n'existe pas
    console.log("user non existant");
  }
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }

}
function deconnexion() {
  try{
    masquerApp();
  masquerAppAdmin();
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
  
}

function addUser(user) {
  try{
   this._listUsers.push(user); 
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
  
}

function createUser(username, password, admin) {
  try{
     if (username == undefined && password == undefined) {
    var username = document.getElementById("nom-utilisateur").value;
    var password = document.getElementById("mot-de-passe").value;
  }

  if (!getUser(username, password)) {
    let user = new Utilisateur(username, password, admin);
    console.log(user);
    addUser(user);
  } else {
    console.log("utilisateur existe déja");
  }
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
 
}

function getUser(username, password) {
  try{
     var returnUser = false;
  _listUsers.forEach((user) => {
    if (user.username() === username && user.password() === password) {
      returnUser = user;
    }
  });

  return returnUser;
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
 
}

function afficherAppUser() {
  try{
     initAnnonces();
  for (let element of document.getElementsByClassName("connecte")) {
    element.classList.remove("hidden");
    element.classList.add("visible");
  }
  for (let element of document.getElementsByClassName("deconnecte")) {
    element.classList.remove("visible");
    element.classList.add("hidden");
  }
  document.getElementById("bouton-ajouter-annonce").style.visibility =
    "visible";
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
 
}

function masquerApp() {
  try{
    for (let element of document.getElementsByClassName("connecte")) {
    element.classList.remove("visible");
    element.classList.add("hidden");
  }
  for (let element of document.getElementsByClassName("deconnecte")) {
    element.classList.remove("hidden");
    element.classList.add("visible");
  }
  document.getElementById("bouton-ajouter-annonce").style.visibility = "hidden";
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
  
}
function ajouterAnnonce() {
  try{
    for (let element of document.getElementsByClassName("ajout-annonce")) {
    element.classList.remove("hidden");
    element.classList.add("visible");
  }

  document.getElementById("text-annonce").innerHTML = "";
  document.getElementById("bouton-ajouter-annonce").style.visibility = "hidden";
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
  
}
function annulerAjoutAnnonce() {
  try{
     document.getElementById("bouton-ajouter-annonce").style.visibility =
    "visible";
  for (let element of document.getElementsByClassName("ajout-annonce")) {
    element.classList.remove("visible");
    element.classList.add("hidden");
  }
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
 
}

function initAnnonces() {
  try{
     document.getElementById("liste-annonces").innerHTML = "";
  _listeAnnonce.forEach((element) => {
    let node = document.createElement("LI");
    let textNode = document.createTextNode(element.text());
    node.appendChild(textNode);

    if (element.user() === userConnecte?.username()) {
      let boutonSupprimer = document.createElement("button");
      let boutonSupprimerText = document.createTextNode("supprimer");
      boutonSupprimer.id = element.id();
      boutonSupprimer.classList.add("bouton-supprimer-liste");
      boutonSupprimer.appendChild(boutonSupprimerText);
      node.appendChild(boutonSupprimer);
      boutonSupprimer.onclick = () => {
        supprimerAnnonce(element.id());
      };
      boutonSupprimer.classList.add("connecte");
    } else {
    }

    document.getElementById("liste-annonces").appendChild(node);
  });
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
 
}

function supprimerAnnonce(id) {
  try{
    var index = id - 1;
  _listeAnnonce.splice(index, 1);
  initAnnonces();
  console.log("supprimer annonce", index, _listeAnnonce);
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
  
}
function soumettreAnnonce() {
  try{
    var textAnnonce = document.getElementById("text-annonce").value;

  if (textAnnonce != "") {
    _listeAnnonce.push(
      new Annonce(_listeAnnonce.length, textAnnonce, userConnecte.username())
    );

    initAnnonces();
  }
  document.getElementById("bouton-ajouter-annonce").style.visibility =
    "visible";
  for (let element of document.getElementsByClassName("ajout-annonce")) {
    element.classList.remove("visible");
    element.classList.add("hidden");
  }

  document.getElementById("text-annonce").innerHTML = "";
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
  
}

function afficherAppAdmin() {
  try{
   for (let element of document.getElementsByClassName("admin")) {
    element.classList.remove("hidden");
    element.classList.add("visible");
  }
  document.getElementById("bouton-supprimer-utilisateur").style.visibility =
    "visible";
  document.getElementById("bouton-ajouter-utilisateur").style.visibility =
    "visible"; 
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
  
}

function masquerAppAdmin() {
  try{
     for (let element of document.getElementsByClassName("admin")) {
    element.classList.remove("visible");
    element.classList.remove("visible-ajout");
    element.classList.add("hidden");
  }
  document.getElementById("bouton-supprimer-utilisateur").style.visibility =
    "hidden";
  document.getElementById("bouton-ajouter-utilisateur").style.visibility =
    "hidden";
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
 
}

function adminAjouterUtilisateur() {
  try{
    for (let element of document.getElementsByClassName(
    "form-ajout-utilisateur"
  )) {
    element.classList.remove("hidden");
    element.classList.remove("hidden-ajout");
    element.classList.add("visible-ajout");
  }
  document.getElementById("bouton-ajouter-utilisateur").style.visibility =
    "hidden";
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
  
}

function adminSoumettreNouveauUtilisateur() {
  try{
    for (let element of document.getElementsByClassName(
    "form-ajout-utilisateur"
  )) {
    element.classList.remove("visible-ajout");
    element.classList.add("hidden-ajout");
  }
  document.getElementById("bouton-ajouter-utilisateur").style.visibility =
    "visible";
  document.getElementById("bouton-supprimer-utilisateur").style.visibility =
    "visible";

  var username = document.getElementById("admin-nom-utilisateur").value;
  var password = document.getElementById("admin-mot-de-passe").value;
  var admin = document.getElementById("bool-admin").checked;

  createUser(username, password, admin);
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
  
}

function adminAnnulerSoumettreUtilisateur() {
  try{
     for (let element of document.getElementsByClassName(
    "form-ajout-utilisateur"
  )) {
    element.classList.remove("visible-ajout");
    element.classList.add("hidden-ajout");
  }
  document.getElementById("bouton-ajouter-utilisateur").style.visibility =
    "visible";
  document.getElementById("bouton-supprimer-utilisateur").style.visibility =
    "visible";
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
 
}

function deleteUser() {
  try{
    for (let element of document.getElementsByClassName(
    "form-supprimer-utilisateur"
  )) {
    element.classList.remove("hidden-ajout");
    element.classList.add("visible-ajout");
  }
  document.getElementById("bouton-supprimer-utilisateur").style.visibility =
    "hidden";
}catch(e){
  listeErreurs.push(new Erreur(e.name, e.message));

}
  
}

function onclickDeleteUser() {
  try{
     let username = document.getElementById("admin-nom-utilisateur").value;

  for (let i = 0; i < _listUsers.length; i++) {
    if (_listUsers[i].username() == username) {
      console.log("suppression de " + _listUsers[i].username());
      _listUsers.splice(i, 1);
    }
  }

  console.log("");
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
 
}

function annulerDeleteUser() {
  try{
    for (let element of document.getElementsByClassName(
      "form-supprimer-utilisateur"
    )) {
      element.classList.remove("visible-ajout");
      element.classList.add("hidden-ajout");
    }
    document.getElementById("bouton-supprimer-utilisateur").style.visibility =
      "visible";
  }catch(e){
    listeErreurs.push(new Erreur(e.name, e.message));
  }
 
}


function afficherErreurs(){
  string = "LISTE DES ERREURS\n\n";
 
  if(listeErreurs.length == 0){
    string = "Aucune erreur à afficher."
  }else{
    for (var j = 0; j < listeErreurs.length ; j++)
      {
          string += listeErreurs[j].nom()+ ": " + listeErreurs[j].message() + "\n";
      }
      string += "\n\n";
  }
      

  alert(string);

}
